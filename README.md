# 通过 cli 模式执行脚本，
# 例： php transCharsets.php your_abs_dir [utf-8] 
# your_abs_dir 要转换字符集的目录（绝对路径）
# 第二个参数是要转换后的字符集，默认为 utf-8

# 注意：在使用前请备份要转换的目录，因为执行该脚本后会覆盖原文件