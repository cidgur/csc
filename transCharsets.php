<?php

$arg_arr = $argv;
$default_arr = [
	'1' => '', // dir
	'2' => 'UTF-8', // charset
];
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

$default_arr = array_insert_by_key($default_arr, $arg_arr);
$charset = $default_arr['2'];
$path = $default_arr['1'];
if (empty($path) || !is_dir($path)) {
	outError('Dir is not exists!');
}

transCharset($path, $charset);
exit('ok');
/**
 * 转换字符集
 */
function transCharset($path, $charset) {
	$fh = opendir($path);
	if ($fh) {
		while ($read = readdir($fh)) {
			if (in_array($read, ['.', '..'])) {
				continue;
			}
			$file = $path . DS . $read;
			if (is_dir($file)) {
				transCharset($file, $charset);
			} else {
				$prefix = getSourcePrefix($file);
				if (in_array($prefix, ['html', 'php', 'txt', 'sql'])) {
					$content = file_get_contents($file);
					$origin_set = mb_detect_encoding($content, ['ASCII', 'GBK', 'UTF-8', 'UTF-16LE', 'UTF-16BE', 'ISO-8859-1']);
					// var_dump($file, $origin_set, $charset);exit;
					if ($origin_set && $origin_set != $charset) {
						printf("File name: %s, Origin_set: %s \n", $file, $origin_set);
						$content = mb_convert_encoding($content, $charset, $origin_set);
						if ($content) {
							if ($prefix != 'sql') {
								file_put_contents($file, "\xEF\xBB\xBF" . $content);
							} else {
								file_put_contents($file, $content);
							}
						}
					}
				}
			}
		}
	}
	closedir($fh);
}

/**
 * 合并数组 通过key
 */
function array_insert_by_key($default, $arr) {
	foreach ($arr as $key => $value) {
		$default[$key] = $value;
	}
	return $default;
}

/**
 * 输出错误
 */
function outError($msg, $code=-1) {
	$trace = debug_backtrace();
	$msg = "Error number: $code; Error message: $msg \n#trace:\n";
	foreach ($trace as $index => $t) {
		$msg .= "#$index:\n";
		foreach ($t as $k => $item) {
			# code...
			if ($k == 'args') {
				$item = implode(',', $item);
			}
			$msg .= " $k : $item \n";
		}
		
	}
	exit($msg);
}

/**
 * 获取资源后缀
 */
function getSourcePrefix($file_url) {
	$arr = explode('.', $file_url);
	return array_pop($arr);
}
